# Mini panels access

#### Description
Like suggested by [this issue](https://www.drupal.org/node/1289830), this
module adds an _Access_ tab to the mini panel UI. This gives the user the
ability to define access restrictions to mini panels. Out of the box this
access restrictions are applied to blocks.

#### Modules using it

- Mini panels token