<?php

class panels_mini_ui_access extends panels_mini_ui {

  function edit_form_access(&$form, &$form_state) {
    ctools_include('plugins', 'panels');
    ctools_include('context-access-admin');
    ctools_include('context');
    
    $item = $form_state['item'];
    
    $cache = panels_mini_access_cache_get($item->name);
    
    $form_state['module'] = 'panels_mini';
    $form_state['callback argument'] = $item->name;
    $form_state['access'] = $cache->access;
    $form_state['no buttons'] = TRUE;
    
    $form_state['contexts'] = ctools_context_load_contexts($cache);

    $form = ctools_access_admin_form($form, $form_state);
    
    if($form_state['form type'] == 'add'){
      $form['add-button']['add-url'];
    }
    
    return;
  }

  function edit_form_access_submit(&$form, &$form_state) {
    if($form_state['form type'] != 'add') {
      self::_edit_form_access_submit($form, $form_state);
    }
  }
  
  function _edit_form_access_submit(&$form, &$form_state) {
    $item = &$form_state['item'];
    if($item->access) {
      $item->access['logic'] = $form_state['values']['logic'];
      $item->display->panel_settings['access'] = $item->access;
    }
  }
  
  function edit_form_layout_submit(&$form, &$form_state) {
    parent::edit_form_layout_submit($form, $form_state);
    // saving of access settings shoulb be done here because we want
    // to save it in display and a new display is created in
    // edit_form_layout_submit() method, so we have to do is this way.
    if($form_state['form type'] == 'add'){
      self::_edit_form_access_submit($form, $form_state);
    }
  }
  
}
