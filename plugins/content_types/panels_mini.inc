<?php

$plugin = ctools_get_content_type('panels_mini');
$plugin['render callback'] = 'panels_mini_access_panels_mini_content_type_render';
$plugin['admin info'] = 'panels_mini_access_panels_mini_content_type_admin_info';

function panels_mini_access_panels_mini_content_type_render($subtype, $conf, $panel_args, &$contexts){
  $panel_mini = panels_mini_load($subtype);
  if(panels_mini_access_check_access($panel_mini, $contexts)){
    return panels_mini_panels_mini_content_type_render($subtype, $conf, $panel_args, $contexts);
  }
}

function panels_mini_access_panels_mini_content_type_admin_info($subtype, $conf){
  $block = panels_mini_panels_mini_content_type_admin_info($subtype, $conf);
  if(isset($block)){
    $panels_mini_export_ui_plugin = ctools_get_plugins('ctools', 'export_ui', 'panels_mini');
    $order = $panels_mini_export_ui_plugin['form info']['order'];
    
    $links = array();
    foreach ($order as $tail => $title) {
      $links[] = l($title, 'admin/structure/mini-panels/list/' . $subtype . '/edit/' . $tail, array('query' => drupal_get_destination()));
    }
    
    $block->content = theme('item_list', array('items' => $links));
    return $block;
  }
}